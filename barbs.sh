#!/bin/sh
# Brian's Auto Rice Boostrapping Script (LARBS)
# by Brian Fitzpatrick <bfitzpat@math.duke.edu>
# License: GNU GPLv3

### OPTIONS AND VARIABLES ###

while getopts ":a:r:b:p:h" o;
do case "${o}" in
       h) printf "Optional arguments for custom use:\\n  -r: Dotfiles repository (local file or url)\\n  -p: Dependencies and programs csv (local file or url)\\n  -a: AUR helper (must have pacman-like syntax)\\n  -h: Show this message\\n" && exit 1 ;;
       r) dotfilesrepo=${OPTARG} && git ls-remote "$dotfilesrepo" || exit 1 ;;
       b) repobranch=${OPTARG} ;;
       p) progsfile=${OPTARG} ;;
       a) aurhelper=${OPTARG} ;;
       *) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
   esac
done

[ -z "$dotfilesrepo" ] && dotfilesrepo="https://github.com/lukesmithxyz/voidrice.git"
[ -z "$progsfile" ] && progsfile="https://raw.githubusercontent.com/LukeSmithxyz/LARBS/master/progs.csv"
[ -z "$aurhelper" ] && aurhelper="paru"
[ -z "$repobranch" ] && repobranch="master"

### FUNCTIONS ###

installpkg() { pacman --noconfirm --needed -S "$1" >/dev/null 2>&1 ;}

error() { clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;}

prettypac() { \
	      grep -q "^Color" /etc/pacman.conf || sed -i "s/^#Color$/Color/" /etc/pacman.conf
	      grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf ;}

updatemir () { reflector --verbose --latest 5 --protocol https --country US --sort rate --save /etc/pacman.d/mirrorlist >/dev/null 2>&1 ;}

selectdsk() { disk=$(dialog --menu "Select the disk for $bios partitioning." 0 0 0 $(lsblk -lpnSo NAME,SIZE,TRAN | awk '! /usb$/ {print $1,$2}') 3>&1 1>&2 2>&3 3>&-) ;}

partitiondsk() { \
		 dialog --yesno "Do you want to partition $disk according to $bios?" 0 0 || error "User decided not to partition $disk."

		 dialog --yesno "Are you sure you want to partition $disk according to $bios? This will wipe all the data on $disk!" 0 0 || error "User chickened out of partitioning $disk."

		 dialog --infobox "Partitioning $disk according to $bios." 0 0

		 [ "$bios" = "efi" ] && echo ';' | sfdisk $1
		 [ "$bios" = "uefi" ] && sgdisk -Z $1 && sgdisk -n 0:0:+450M -t 0:ef00 -c 0:"EFI system partition" $1 && sgdisk -n 0:0:0 -t 0:8300 -c 0:"Linux filesystem" $1 ;}

dskpart() { lsblk -nlpo NAME,TYPE -x SIZE $disk | awk ' /part/ {print $1}' | head -n $1 | tail -1 ;}

fmtparts() { \
	     [ "$bios" = "efi" ] && mkfs.ext4 $(dskpart 1)
	     [ "$bios" = "uefi" ] && mkfs.fat -F32 $(dskpart 1) && mkfs.ext4 $(dskpart 2) ;}

mntparts() { \
	     [ "$bios" = "efi" ]  && mount $(dskpart 1) /mnt
	     [ "$bios" = "uefi" ] && mount $(dskpart 2) /mnt && mkdir -p /mnt/boot && mount $(dskpart 1) /mnt/boot ;}

### CONSTANTS ###

ls /sys/firmware/efi/efivars >/dev/null 2>&1 && bios="uefi" || bios="efi"

### PRE CHROOT FUNCTION ###

prechroot() { \
	      # welcome the user
	      dialog --title "Welcome!" --msgbox "Welcome to Brian's Auto-Rice Bootstrapping Script!\\n\\nThis script will automatically install a fully-featured Linux desktop.\\n\\n-Brian" 0 0 || error "User exited."

	      # give the user a chance to back out
	      dialog --colors --title "Install arch linux?" --yes-label "Proceed" --no-label "Abort!" --yesno "You are logged into the arch iso and about to install arch linux. Do you want to proceed with the installation?" 0 0 || error "User exited."

	      # prep for install
	      dialog --infobox "Prepping for install. Right now pacman is updating, dialog is being installed, and the time is being set." 0 0 && pacman -Sy >/dev/null 2>&1 && updatemir && installpkg archlinux-keyring dialog && timedatectl net-ntp true || error "User exited."

	      # select disk (as $disk variable)
	      selectdsk || error "User exited."

	      # partition $disk according to $bios
	      partitiondsk || error "User exited."

	      # format $disk according to $bios
	      dialog --infobox "Formatting $disk according to $bios." 0 0 && fmtparts || error "User exited."

	      # mount $disk partitions according to $bios
	      dialog --infobox "Mounting partitions on $disk according to $bios." 0 0 && mntparts || error "User exited."

	      # install the bare essentials
	      dialog --infobox "Installing the bare essentials." 0 0 && pacstrap /mnt base base-devel linux linux-firmware dialog || error "User exited."

	      # generate /mnt/etc/fstab
	      dialog --infobox "Generating /mnt/etc/fstab" 0 0 && genfstab -U /mnt >> /mnt/etc/fstab || error "User exited."

	      # copy barbs.sh into /mnt/root/barbs.sh
	      dialog --infobox "Copying barbs.sh into /mnt/root/barbs.sh" && cp barbs.sh /mnt/root/barbs.sh || error "User exited."



}

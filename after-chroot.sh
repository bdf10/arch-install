#!/bin/sh
# Brian's pre-chroot arch install script.
# by Brian Fitzpatrick <bfitzpat@math.duke.edu>
# License: GNU GPLv3

### OPTIONS AND VARIABLES ###

while getopts ":n:p:h" o;
do case "${o}" in
       h) printf "Arguments for custom use:\\n  -n: Host name.\\n  -p: Host password.\\n  -d: Location of the disk to install to.\\n  -h: Show this message\\n" && exit 1 ;;
       n) host_name=${OPTARG} ;;
       p) host_password=${OPTARG} ;;
       *) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
   esac
done

### FUNCTIONS ###

error() { printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;}

find_disk () { lsblk -npo pkname $(findmnt -no SOURCE /) ;}

prettify_pacman () { \
		     grep -q "^Color" /etc/pacman.conf || sed -i "s/^#Color$/Color/" /etc/pacman.conf
		     grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf ;}

update_mirrors () { reflector --verbose --latest 5 --protocol https --country US --sort rate --save /etc/pacman.d/mirrorlist ;}

set_time () { ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime && hwclock --systohc ;}

generate_locale () { sed -i '177s/.//' /etc/locale.gen && locale-gen && echo "LANG=en_US.UTF-8" >> /etc/locale.conf ;}

birth_host () { \
		# Prompts user for host name and password.
		host_name=$(dialog --inputbox "First, please enter the name of the machine." 10 60 3>&1 1>&2 2>&3 3>&1) || exit 1
		while ! echo "$host_name" | grep -q "^[a-z_][a-z0-9_-]*$"; do
		    host_name=$(dialog --no-cancel --inputbox "Host name not valid. Give a host name beginning with a letter, with only lowercase letters, - or _." 10 60 3>&1 1>&2 2>&3 3>&1)
		done
		echo $host_name > /etc/hostname
		echo -e "127.0.0.1\tlocalhost\n::1\tlocalhost\n127.0.1.1\t$host_name.localdomain\t$host_name" > /etc/hosts

		pass1=$(dialog --no-cancel --passwordbox "Enter a root password." 10 60 3>&1 1>&2 2>&3 3>&1)
		pass2=$(dialog --no-cancel --passwordbox "Retype root password." 10 60 3>&1 1>&2 2>&3 3>&1)
		while ! [ "$pass1" = "$pass2" ]; do
		    unset pass2
		    pass1=$(dialog --no-cancel --passwordbox "Passwords do not match.\\n\\nEnter root password again." 10 60 3>&1 1>&2 2>&3 3>&1)
		    pass2=$(dialog --no-cancel --passwordbox "Retype root password." 10 60 3>&1 1>&2 2>&3 3>&1)
		done
		echo "root:$pass1" | chpasswd
		unset pass1 pass2 ;}

install_packages () { pacman -S grub networkmanager base-devel emacs-nox nvim dash systemd-swap cargo bluez bluez-utils bat lsd acpid intel-ucode --noconfirm ;}

install_grub () { \
		  # install grub on $1 according to $2={efi,uefi}
		  [ "$2" = "efi" ] && installpkg grub && grub-install --target=i386-pc "$1"
		  [ "$2" = "uefi" ] && installpkg "grub efibootmgr" && grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
		  grub-mkconfig -o /boot/grub/grub.cfg ;}

configure_reflector () { echo -e "--save /etc/pacman.d/mirrorlist\n--country US\n--protocol https\n--sort rate\n--latest 10" > /etc/xdg/reflector/reflector.conf ;}

start_systemd_services () { \
			    systemctl enable NetworkManager.service
			    systemctl enable bluetooth.service
			    systemctl enable reflector.service
			    systemctl enable acpid.service ;}

exit_msg () { printf "\e[1;32mArch has been installed!\e[0m\n" ;}

### VARIABLES ###

ls /sys/firmware/efi/efivars >/dev/null 2>&1 && bios="uefi" || bios="efi"

### SCRIPT ###
prettify_pacman
update_mirrors
set_time
generate_locale
birth_host
install_packages
install_grub "$(find_disk)" "$bios"
configure_reflector
start_systemd_services
exit_msg

#!/bin/sh
# Brian's arch install script.
# by Brian Fitzpatrick <bfitzpat@math.duke.edu>
# License: GNU GPLv3

### OPTIONS AND VARIABLES ###

while getopts ":d:h" o;
do case "${o}" in
       h) printf "Arguments for custom use:\\n  -d: Location of the disk to install to.\\n  -h: Show this message\\n" && exit 1 ;;
       d) disk=${OPTARG} ;;
       *) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
   esac
done

### FUNCTIONS ###

error() { printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;}

prettify_pacman () {
    grep -q "^Color" /etc/pacman.conf || sed -i "s/^#Color$/Color/" /etc/pacman.conf
    grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
}

update_mirrors () { reflector --verbose --latest 5 --protocol https --country US --sort rate --save /etc/pacman.d/mirrorlist ;}

install_dialog () { pacman -Sy dialog --noconfirm ;}

nth_partition () {
    # return location of partition number $2 of disk $1
    echo $(lsblk -pnlo NAME,TYPE "$1" | grep "part$" | awk '{print $1}' | sed ""$2"q;d")
}

n_partitions () { lsblk -nplo NAME,TYPE $1 | grep -c "part$" ;}

fs_type () { lsblk -nlo FSTYPE $1 ;}

nth_fs_type () { fs_type "$(nth_partition $1 $2)";}

partition_disk () {
    # partitions disk $1 according to $2={efi,uefi}

    # check if user wants to partition the disk
    dialog --yesno "Do you want to partition the disk $1 according to $2?" 8 32 || return

    [ "$2" = "efi" ] && [ "$(n_partitions $1)" -eq 1 ] && [ "$(nth_fs_type $1 1)" = "ext4" ] && return
    [ "$2" = "uefi" ] && [ "$(n_partitions $1)" -eq 2 ] && [ "$(nth_fs_type $1 1)" = "vfat" ] && [ "$(nth_fs_type $1 2)" = "ext4" ] && return

    [ "$2" = "efi" ] && echo ';' | sfdisk $1
    [ "$2" = "uefi" ] && sgdisk -Z $1 && sgdisk -n 0:0:+450M -t 0:ef00 -c 0:"EFI system partition" $1 && sgdisk -n 0:0:0 -t 0:8300 -c 0:"Linux filesystem" $1
}

format_partitions () {
    # formats partitions on $1 according to $2={efi,uefi}

    # check if user wants to format the partitions
    dialog --yesno "Do you want to format the partitions on $1 according to $2?" 8 32 || return

    [ "$2" = "efi" ] && [ "$(n_partitions $1)" -eq 1 ] && [ "$(nth_fs_type $1 1)" = "ext4" ] && return
    [ "$2" = "uefi" ] && [ "$(n_partitions $1)" -eq 2 ] && [ "$(nth_fs_type $1 1)" = "vfat" ] && [ "$(nth_fs_type $1 2)" = "ext4" ] && return

    [ "$2" = "efi" ] && mkfs.ext4 $(nth_partition "$1" 1)
    [ "$2" = "uefi" ] && mkfs.fat -F32 $(nth_partition "$1" 1) && mkfs.ext4 $(nth_partition "$1" 2)
}

mount_partitions () {
    # mounts disk $1 according to specification $2={efi,uefi}
    [ "$2" = "efi" ]  && mount $(nth_partition "$1" 1) /mnt
    [ "$2" = "uefi" ] && mount $(nth_partition "$1" 2) /mnt && mkdir -p /mnt/boot && mount $(nth_partition "$1" 1) /mnt/boot
}

enter_chroot () {
    # enter chroot and run install script
    # $1=$disk (-d flag)
    # $2=$host_name (-n flag)
    # $3=$host_password (-p flag)
    cp after-chroot.sh /mnt/root/after-chroot.sh
    arch-chroot /mnt /root/after-chroot.sh
}

### VARIABLES ###

[ -z "$disk" ] && disk=$(find_disk)
ls /sys/firmware/efi/efivars >/dev/null 2>&1 && bios="uefi" || bios="efi"

### SCRIPT ###
prettify_pacman
update_mirrors
install_dialog
timedatectl set-ntp true
partition_disk "$disk" "$bios"
format_partitions "$disk" "$bios"
mount_partitions "$disk" "$bios"
pacstrap /mnt base linux linux-firmware reflector nano
genfstab -U /mnt >> /mnt/etc/fstab
enter_chroot

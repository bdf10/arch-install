#!/usr/bin/env sh

prettify_pacman () {
    sed -i 's/#Color/Color/g ; s/#VerbosePkgLists/#VerbosePkgLists\nILoveCandy/g' /etc/pacman.conf
}

initialize_zsh_config () {
    ZSH_CONFIG=$1/.config/zsh/.zshrc
    mkdir -p "${ZSH_CONFIG%/*}"

    cat > $ZSH_CONFIG <<EOF
# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
setopt autocd		# Automatically cd into typed directory.
setopt interactive_comments

# History in cache directory:
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=~/.cache/zsh/history

# Load aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

command -v starship >/dev/null && eval "$(starship init zsh)"
EOF

}

initialize_emacs_config () {
    EMACS_CONFIG=$1/.config/emacs/init.el
    mkdir -p "${EMACS_CONFIG%/*}"

    cat > $EMACS_CONFIG <<EOF
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(global-linum-mode)

(setq-default indent-tabs-mode nil)
(setq-default fill-column 80)
(setq-default blink-cursor-mode nil)

(setq
 backup-by-copying t
 backup-directory-alist '(("." . "~/.local/share/emacs-saves"))
 auto-save-file-name-transforms `(("." ,temporary-file-directory t))
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)
EOF

}

before_chroot () {

    reflector --verbose --latest 10 --protocol https --country US --sort rate --save /etc/pacman.d/mirrorlist

    prettify_pacman

    timedatectl set-ntp true

    # partition disks
    DISK=/dev/nvme0n1
    sgdisk -Z $DISK
    sgdisk -n 0:0:+350M -t 0:ef00 -c 0:"EFI system partition" $DISK
    sgdisk -n 0:0:0 -t 0:8300 -c 0:"Linux filesystem" $DISK

    mkfs.fat -F32 "$DISK"p1
    mkfs.ext4 "$DISK"p2

    mount "$DISK"p2 /mnt

    mkdir -p /mnt/boot/efi
    mount "$DISK"p1 /mnt/boot/efi

    pacstrap /mnt base linux linux-firmware

    genfstab -U /mnt >> /mnt/etc/fstab

}

after_chroot () {

    prettify_pacman

    ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
    hwclock --systohc
    sed -i '177s/.//' /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf

    clear
    echo "Enter the HOST NAME."
    read HOST
    echo
    echo $HOST >> /etc/hostname
    echo "127.0.0.1 localhost" >> /etc/hosts
    echo "::1       localhost" >> /etc/hosts
    echo "127.0.1.1 $HOST.localdomain $HOST" >> /etc/hosts

    clear
    echo "Enter the ROOT PASSWORD."
    read ROOTPASS
    echo
    echo root:$ROOTPASS | chpasswd

    pacman -S grub efibootmgr networkmanager reflector base-devel xdg-user-dirs xdg-utils neovim emacs-nox alsa-utils opendoas zsh openssh dash git systemd-swap cargo bluez bluez-utils bat lsd acpid zsh-syntax-highlighting --noconfirm

    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
    grub-mkconfig -o /boot/grub/grub.cfg

    sed -i 's/#swapfc_enabled=0/swapfc_enabled=1/g' /etc/systemd/swap.conf

    echo "--save /etc/pacman.d/mirrorlist" > /etc/xdg/reflector/reflector.conf
    echo "--country US" >> /etc/xdg/reflector/reflector.conf
    echo "--protocol https" >> /etc/xdg/reflector/reflector.conf
    echo "--sort rate" >> /etc/xdg/reflector/reflector.conf
    echo "--latest 10" >> /etc/xdg/reflector/reflector.conf

    systemctl enable NetworkManager
    systemctl enable sshd
    systemctl enable bluetooth
    systemctl enable reflector.service
    systemctl enable systemd-swap
    systemctl enable acpid

    echo "vm.swappiness=0" > /etc/sysctl.d/99-swappiness.conf

    ln -sfT dash /usr/bin/sh

    chsh -s /bin/zsh
    initialize_zsh_config $HOME

    initialize_emacs_config $HOME

    clear
    echo "Enter the USER NAME."
    read USER
    echo
    useradd -m $USER

    clear
    echo "Enter the USER PASSWORD."
    read USERPASS
    echo
    echo $USER:$USERPASS | chpasswd

    echo "permit $USER as root" > /etc/doas.conf

    chsh -s /bin/zsh $USER

    pacman -Rns sudo --noconfirm

    cp $(realpath $0) /home/$USER

    printf "\e[1;32mArch has been installed!\e[0m\n"

}

after_login () {
    clear
    echo "Enter the WIFI NAME."
    read WIFINAME

    clear
    echo "Enter the WIFI PASSWORD."
    read WIFIPASS

    nmcli device wifi connect $WIFINAME password $WIFIPASS

    doas -- pacman -Syyu --noconfirm

    initialize_zsh_config $HOME
    initialize_emacs_config $HOME

    git clone https://aur.archlinux.org/paru.git /tmp/paru
    cd /tmp/paru && makepkg -si
    doas sed -i '33s/.// ; 36s/.// ; s/SudoLoop/#SudoLoop/g' /etc/paru.conf

    paru -S dashbinsh starship otf-nerd-fonts-fira-code --noconfirm

}

case "$1" in
    before-chroot) before_chroot ;;
    after-chroot) after_chroot ;;
    after-login) after_login ;;
    *) cat <<EOF
install-uefi: automated arch linux installation

Allowed options:
	before-chroot	prep system
	after-chroot	install system
	after-login	configure system
EOF
esac
